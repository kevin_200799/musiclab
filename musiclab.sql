-- phpMyAdmin SQL Dump
-- version 5.0.0-alpha1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 05-11-2019 a las 21:04:31
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `musiclab`
--
CREATE DATABASE IF NOT EXISTS `musiclab` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `musiclab`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albumnes`
--

CREATE TABLE `albumnes` (
  `id_album` int(11) NOT NULL,
  `titulo_album` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `f_lanzamiento` date NOT NULL,
  `id_artista` int(11) NOT NULL,
  `portada` varchar(10000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `albumnes`
--

INSERT INTO `albumnes` (`id_album`, `titulo_album`, `f_lanzamiento`, `id_artista`, `portada`) VALUES
(1, 'Origen (Vol 1)', '2020-03-11', 1, 'dm.png'),
(2, 'Extranjera', '2010-03-16', 1, 'extranhjera.png'),
(3, 'Sin Fronteras', '2014-04-01', 1, 'sin_fronteras_DM_cover.png'),
(4, 'Para Olvidarte De Mí', '2009-01-01', 2, NULL),
(5, 'Rebelde (Edicao Brasil)', '2019-03-03', 2, NULL),
(6, 'Imperfect', '2008-05-05', 5, NULL),
(7, 'The Best Of Damn Tour (Live In Toronto)', '2008-12-20', 4, 'thebestofdanmtour.png'),
(8, 'The Best Of Damn Thing (Edition Especial)', '2008-03-02', 4, 'thebestofdanmthingeditespecial.png'),
(9, 'DM', '2016-03-14', 1, 'dm2.png'),
(10, 'Let Go', '2002-04-04', 4, 'Let_GO_Avril Lavigne.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artistas`
--

CREATE TABLE `artistas` (
  `id_artista` int(11) NOT NULL,
  `nombres` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `seudo` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `nacimiento` date NOT NULL,
  `biografia` text COLLATE utf8_spanish2_ci NOT NULL,
  `id_disquera` int(11) DEFAULT NULL,
  `foto` varchar(10000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `artistas`
--

INSERT INTO `artistas` (`id_artista`, `nombres`, `apellidos`, `seudo`, `nacimiento`, `biografia`, `id_disquera`, `foto`) VALUES
(1, 'Dulce María', 'Espinoza Serviñon', 'Dulce María', '1985-12-06', 'Dulce María Espinosa Saviñón, conocida como Dulce María, es una cantante, actriz y compositora mexicana', 2, 'dm2019.jpg'),
(2, 'RBD', '', 'RBD', '2004-12-01', 'Fue un grupo mexicano nacido de la telenovela Rebelde', 2, 'rbd.jpg'),
(4, 'Avril Ramona', 'Lavigne', 'Avril Lavigne', '1984-09-27', 'Avril Ramona Lavigne​ ​ es una cantante, compositora, diseñadora de moda productora musical, actriz y filántropa canadiense. Algunos críticos de revistas como Billboard la llaman la princesa del pop punk.​', 2, 'Avril_Lavigne.jpg'),
(5, 'Natasha', 'Dueñas', 'Natasha', '1990-12-12', 'Natasha Dueñas\nNacio en Arguentina', 2, 'natasha_duenas.jpg'),
(6, 'Alex', 'Bustamante', 'Alex', '1999-09-29', 'Salvadorean', 1, 'nulo.png'),
(7, 'Bryan Bryan', 'G Gonzales', 'RyanKFC', '1996-01-21', 'It\'s real?', 2, 'sinPefilPhoto.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canciones`
--

CREATE TABLE `canciones` (
  `id_cancion` int(11) NOT NULL,
  `titulo` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `id_album` int(11) NOT NULL,
  `lanzamiento` date NOT NULL,
  `duracion` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL,
  `id_genero` int(11) NOT NULL,
  `file` varchar(10000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `portada_single` varchar(10000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `canciones`
--

INSERT INTO `canciones` (`id_cancion`, `titulo`, `id_album`, `lanzamiento`, `duracion`, `id_genero`, `file`, `portada_single`) VALUES
(3, 'Extranjera', 2, '2011-03-02', '03:11', 4, 'Dulce Maria Extranjera.mp3', NULL),
(4, 'Dejarte de amar', 9, '2016-11-11', '03:13', 4, 'Dulce maria Dejarte De Amar.mp3', NULL),
(5, 'Te Daría Todo (Versión Preview)', 1, '2019-11-11', '04:00', 1, 'Dulce Maria - Te Daria Todo 2019 (Audio Origen)(MP3_160K).mp3', NULL),
(6, 'I\'m with you', 10, '2002-04-04', '04:00', 4, 'Avril Lavigne - I\'m with You.mp3', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disquera`
--

CREATE TABLE `disquera` (
  `id_disquera` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish2_ci NOT NULL,
  `fundacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `disquera`
--

INSERT INTO `disquera` (`id_disquera`, `nombre`, `fundacion`) VALUES
(1, 'Universal Music', '1900-12-12'),
(2, 'EMI Music', '1950-08-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id_genero` int(11) NOT NULL,
  `genero` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id_genero`, `genero`) VALUES
(1, 'Folk'),
(2, 'Rock'),
(3, 'Rock & Roll'),
(4, 'POP'),
(5, 'Balada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Usuarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id_user` int(11) NOT NULL,
  `user` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `sexo` text COLLATE utf8_spanish2_ci NOT NULL,
  `nombres` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id_user`, `user`, `password`, `email`, `sexo`, `nombres`, `apellidos`, `fecha_nacimiento`, `id_rol`) VALUES
(1, 'root', 'MTIzNDU2Nzg=', 'root@mail.com', 'Nulo', 'SuperUser', 'ROOT', '0000-00-00', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `albumnes`
--
ALTER TABLE `albumnes`
  ADD PRIMARY KEY (`id_album`),
  ADD KEY `id_artista` (`id_artista`);

--
-- Indices de la tabla `artistas`
--
ALTER TABLE `artistas`
  ADD PRIMARY KEY (`id_artista`),
  ADD KEY `id_disquera` (`id_disquera`);

--
-- Indices de la tabla `canciones`
--
ALTER TABLE `canciones`
  ADD PRIMARY KEY (`id_cancion`),
  ADD KEY `id_artista` (`id_genero`),
  ADD KEY `id_album` (`id_album`);

--
-- Indices de la tabla `disquera`
--
ALTER TABLE `disquera`
  ADD PRIMARY KEY (`id_disquera`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id_genero`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id_user`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `albumnes`
--
ALTER TABLE `albumnes`
  MODIFY `id_album` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `artistas`
--
ALTER TABLE `artistas`
  MODIFY `id_artista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `canciones`
--
ALTER TABLE `canciones`
  MODIFY `id_cancion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `disquera`
--
ALTER TABLE `disquera`
  MODIFY `id_disquera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `albumnes`
--
ALTER TABLE `albumnes`
  ADD CONSTRAINT `albumnes_ibfk_1` FOREIGN KEY (`id_artista`) REFERENCES `artistas` (`id_artista`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `artistas`
--
ALTER TABLE `artistas`
  ADD CONSTRAINT `artistas_ibfk_1` FOREIGN KEY (`id_disquera`) REFERENCES `disquera` (`id_disquera`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `canciones`
--
ALTER TABLE `canciones`
  ADD CONSTRAINT `canciones_ibfk_2` FOREIGN KEY (`id_album`) REFERENCES `albumnes` (`id_album`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `canciones_ibfk_3` FOREIGN KEY (`id_genero`) REFERENCES `generos` (`id_genero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

