<?php
class LoginModel extends CI_Model
{
	public function Exist_User($usuario,$contrasena)
	{
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->join('roles','usuarios.id_rol = roles.id_rol','inner');
		$this->db->where('user',$usuario);
		$this->db->where('password',$contrasena);
		$query = $this->db->get();
		return $query;
	}
}
?>