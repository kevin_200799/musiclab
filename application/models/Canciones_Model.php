<?php 
class Canciones_Model extends CI_Model
{
	public function SelectAllArtist($id)
	{
		$this->db->select('*');
		$this->db->where('id_album',$id);
		$this->db->from('canciones');
		$this->db->join('albumnes','canciones.id_album = albumnes.id_album','inner');
		$sql = $this->db->get();
		return $sql->result();
	}

	public function Canciones()
	{
		$this->db->select('*');
		$this->db->from('canciones');
		$this->db->join('generos','canciones.id_genero = generos.id_genero','inner');
		$this->db->join('albumnes','canciones.id_album = albumnes.id_album','inner');
		$sql1 = $this->db->get();

		$this->db->select('*');
		$this->db->from('albumnes');
		$this->db->join('artistas','albumnes.id_artista = artistas.id_artista','inner');

		$sql = $this->db->get();

		return $sql1->result().$sql->result();
	}

	public function eliminarCancion($id)
	{
		$this->db->where('id_cancion',$id);
		$this->db->delete('canciones');
	}


	public function CancionesByArtist($id)
	{
		$this->db->select('*');
		$this->db->from('canciones');
		$this->db->join('albumnes','canciones.id_album = albumnes.id_album');
		$this->db->where('id_album',$id);
		$sql = $this->db->get();
		return $sql->result();
	}

}
?>