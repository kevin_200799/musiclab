<?php 
class Artistas_Modelo extends CI_Model
{
	public function Artistas()
	{
		$this->db->select('*');
		$this->db->from('artistas');
		$this->db->join('disquera','artistas.id_disquera = disquera.id_disquera','Inner');
		$sql= $this->db->get();
		return $sql->result();
	}

	public function Disquera()
	{
		$this->db->select('*');
		$this->db->from('disquera');
		$sql= $this->db->get();
		return $sql->result();
	}

	public function InsertNew($data)
		{
			return($this->db->Insert('artistas',$data)) ?true:false;
		}	
}
?>