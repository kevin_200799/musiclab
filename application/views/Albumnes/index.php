<div class="container" style="margin-top: 30px;">
	<a href="<?php echo base_url(); ?>Albumnes_Controller/nuevoAlbum" class="btn btn-warning ">Nuevo álbum</a>
	<hr>
	<table class="table table-striped table-bordered table-dark table-hover">
		<thead>
			<tr>
				<th>Titulo del album</th>
				<th>Fecha de lanzamiento</th>
				<th>Artista</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($album as $M): ?>
				<tr>
					<td><?=$M->titulo_album ?></td>
					<td><?=$M->f_lanzamiento ?></td>
					<td><?=$M->nombres." ".$M->apellidos ?></td>
					<td>
						<a href="<?php echo base_url(); ?>Albumnes_Controller//<?=$M->id_album ?>" class="btn btn-primary">Editar</a>
						<a href="<?php echo base_url(); ?>Albumnes_Controller//<?=$M->id_album ?>" class="btn btn-danger">Eliminar</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>