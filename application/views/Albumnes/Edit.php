<center>
	<div class="container" style="margin-top: 30px;align-content: center;">
		<div class="card col-6 text-left">
			<div class="card-header">
				<h5 class="card-title">Nuevo</h5>
			</div>
			<form method="post" action="<?=base_url(); ?>Albumnes_Controller/updateAlbum">
				<div class="card-body">
					<div class="form-row">
						<div class="col-12">
							<input type="hidden" name="id_album" value="<?=$album->id_album ?>">
							<label>Portada</label>
							<input type="file" name="portada" class="form-control">
						</div>
						<div class="col-12">
							<label>Titulo del album</label>
							<input type="text" name="titulo_album" class="form-control" value="<?=$album->titulo_album?>">
						</div>
						<div class="col-12">
							<label>Fecha de lanzamiento</label>
							<input type="date" name="f_lanzamiento" class="form-control" value="<?=$album->f_lanzamiento ?>">
						</div>
						<div class="col-12">
							<label>Artista</label>
							<select name="id_artista" class="form-control">
								<?php foreach ($artistas as $A): ?>
									<option value="<?=$A->id_artista ?>" <?=$A->id_artista == $album->id_artista ? 'selected': '' ?>><?=$A->nombres." ".$A->apellidos ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<input type="submit" value="Guardar" class="btn btn-primary">
					<a href="<?=base_url()."Albumnes_Controller" ?>" class="btn btn-secondary">volver</a>
				</div>
			</form>
		</div>
	</div>
</center>