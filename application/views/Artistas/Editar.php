<style type="text/css">
	img{
		width: 175px;
		height: 180px;
		margin: 10px;
		padding: 10px;
	}
</style>
<center>
	<div class="container" style="margin-top: 10px;">
		<div class="card col-8">
			<div class="card-header">
				<h5 class="card-title">Editar</h5>
			</div>
			<form method="post" action="<?php echo base_url(); ?>Artistas/updateArtista">
				<div class="card-body text-center">

					<div class="form-row">
						<div class="col-md-12">
							<input type="hidden" name="id_artista" value="<?=$artista->id_artista ?>">
							<label>Artista Foto</label>
							<img src="<?php echo base_url(); ?>img/<?=$artista->foto ?>">
							<input type="file" name="foto" class="form-control" value="<?php echo $artista->foto ?>">
						</div>
						<div class="col-md-6">
							<label>Nombres</label>
							<input type="text" name="nombres" class="form-control" value="<?php echo $artista->nombres ?>">
						</div>
						<div class="col-md-6">
							<label>Apellidos</label>
							<input type="text" name="apellidos" class="form-control" value="<?php echo $artista->apellidos ?>">
						</div>
						<div class="col-md-6">
							<label>Seudonimo</label>
							<input type="text" name="seudo" class="form-control" value="<?php echo $artista->seudo ?>">
						</div>
						<div class="col-md-6">
							<label>Fecha de nacimiento</label>
							<input type="date" name="nacimiento" class="form-control" value="<?php echo $artista->nacimiento ?>">
						</div>
						<div class="col-md-6">
							<label>Biografia</label>
							<textarea name="biografia" class="form-control" style="min-height: 100px;max-height: 200px;"><?php echo $artista->biografia; ?></textarea>
						</div>
						<div class="col-md-6">
							<label>Disquera</label>
							<select name="id_disquera" class="form-control">
								<?php foreach ($disquera as $D): ?>
									<option value="<?=$D->id_disquera ?>" <?=$D->id_disquera == $artista->id_disquera ? 'selected': '' ?>>
										<?=$D->nombre;?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>


					</div>
					<div class="card-footer">
						<input type="submit" value="Guardar" class="btn btn-info">
						<a href="<?php echo base_url(); ?>Artistas/Index" class="btn btn-dark">Volver</a>
					</div>
				</form>
			</div>
		</div>
	</center>