<style type="text/css">
	#carta{
		border-radius: 40%;
		border-color: black;
		width: 500px;
		height: 375px;
		
	}
	#imgcart{
		
		width: 200px;
		margin: 10px;
		padding: 10px;
	}
	img{
		width: 175px;
		height: 180px;
		margin: 10px;
		padding: 10px;
	}
	#paragraf{
		
		width: 200px;
		margin: 10px;
		padding: 10px;
	}
</style>
<body>
	<center>
		<div class="container">
			<div class="col-sm-12">
				<h5 class="text-dark" style="float: left;"><b>Lo mas reciente...</b></h5>
				<a class="btn btn-block col-sm-4 btn-warning" style="float: right;" href="<?php echo base_url(); ?>Artistas/Nuevo">+ Nuevo</a>
			</div><br>
			<hr>
			<?php foreach ($artistas as $A): ?>


				<div id="carta">
					<div id="imgcart" style="float: left;">
						<h5 class="text-center"><b><?=$A->seudo;?></b></h5>
						<img src="<?php echo base_url().'img/'.$A->foto; ?>">
					</div>
					<div id="paragraf" class="text-justify" style="float: right;">
						<p><b><?=$A->nacimiento;?></b></p>
						<p>
							<?=$A->biografia;?>
						</p>
						<br>
						<a href="<?php echo base_url(); ?>Albumnes_Controller/GetAllCoverArtist/<?=$A->id_artista;?>" class="btn btn-success">Ver Albumnes...</a>
						<br>
						<a href="<?php echo base_url(); ?>Artistas/Editar/<?=$A->id_artista;?>">Edit</a>
						<a href="<?php echo base_url(); ?>Artistas/Eliminar/<?=$A->id_artista;?>">Eliminar</a>
					</div>
				</div>
				<hr>

			<?php endforeach; ?>
		</div>

	</center>
</body>