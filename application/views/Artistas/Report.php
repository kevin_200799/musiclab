<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>
	<div class="alert alert-primary">
		<b>Powered By</b>
		<div class="alert alert-success col-4">
			<b>MusicLab</b>
		</div>
	</div>
	<div class="alert alert-dark">
		<h5>Reportes de artistas</h5>
	</div>
	<table class="table table-striped table-bordered table-dark border border-danger">
		<thead>
			<tr>
				<th>Names</th>
				<th>Last Names</th>
				<th>Pseudo</th>
				<th>Brith Date</th>
				<th>Disquera</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($artistas as $A): ?>
				<tr>
					<td><?=$A->nombres; ?></td>
					<td><?=$A->apellidos;?></td>
					<td><?=$A->seudo;?></td>
					<td><?=$A->nacimiento;?></td>
					<td><?=$A->nombre;?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</body>
</html>