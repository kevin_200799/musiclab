<center>	
	
	<div class="container" style="margin-top: 10px;">
		<div class="card col-8 bg-prinary mb-3 text-left">
			<div class="card-header">
				<h5 class="card-title">Editar datos</h5>
			</div>
			<div class="card-body">
				<div class="form-row">
					<label>Artista Foto</label>
					<input type="file" id="foto" class="form-control">
				</div>
				<div class="form-row">
					<div class="col-sm-6">
						<label>Nombres</label>
						<input type="text" id="nombres" class="form-control">
					</div>
					<div class="col-sm-6">
						<label>Apellidos</label>
						<input type="text" id="apellidos" class="form-control">
					</div>
				</div>
				<div class="form-row">
					<div class="col-sm-6">
						<label>Seudonimo</label>
						<input type="text" id="seudo" class="form-control">
					</div>
					<div class="col-sm-6">
						<label>Fecha de nacimiento</label>
						<input type="date" id="nacimiento" class="form-control">
					</div>
				</div>
				<div class="form-row">
					<div class="col-sm-6">
						<label>Biografia</label>
						<textarea id="biografia" class="form-control" style="min-height: 100px;max-height: 200px;"></textarea>
					</div>
					<div class="col-sm-6">
						<label>Disquera</label>
						<select id="id_disquera" class="form-control">
							<option>----</option>
							<?php foreach ($disquera as $D): ?>
								<option value="<?=$D->id_disquera ?>"><?=$D->nombre;?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<br>
				<div class="form-row">
					<button class="btn btn-info btn-block" id="SaveNewArtist">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</body>