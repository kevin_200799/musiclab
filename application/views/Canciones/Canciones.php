
<body>

				<table class="table  table-primary table-striped table-bordered table-hover table-responsive">
					<thead>
						<tr>
							<th>Titulo</th>
							<th>Album</th>
							<th>Lanzamiento</th>
							<th>Duracion</th>
							<th>Genero</th>
							<th>Artista</th>
							<th>Archivo</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($canciones as $C): ?>
							<tr>
								<td><?=$C->titulo ;?></td>
								<td><?=$C->titulo_album ;?></td>
								<td><?=$C->lanzamiento ;?></td>
								<td><?=$C->duracion ;?></td>
								<td><?=$C->genero ;?></td>
								<td><?=$C->seudo ;?></td>
								<td>
									<audio src="<?=base_url(); ?>audio/<?=$C->file;?>" preload="none" controls></audio>
								</td>
								<td>
									<a href="<?=base_url(); ?>Canciones/editarCancion/<?=$C->id_cancion;?>" class="btn btn-primary"><i class="fas fa-edit"></i></a>
									<a href="<?=base_url();?>Canciones/eliminarCancion/<?=$C->id_cancion;?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>