<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<style type="text/css">
		/*body{
			border: 10px solid red;
		}*/
	</style>
</head>
<body>
	<div class="alert alert-dark">
		<b>Powered By</b>
		<div class="alert alert-success col-4">
			<b>MusicLab</b>
		</div>
	</div>
	<table class="table table-sm table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Titulo</th>
				<th>Album</th>
				<th>Lanzamiento</th>
				<th>Duracion</th>
				<th>Genero</th>
				<th>Artista</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($canciones as $C): ?>
				<tr>
					<td><?=$C->titulo ;?></td>
					<td><?=$C->titulo_album ;?></td>
					<td><?=$C->lanzamiento ;?></td>
					<td><?=$C->duracion ;?></td>
					<td><?=$C->genero ;?></td>
					<td><?=$C->seudo ;?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</body>
</html>