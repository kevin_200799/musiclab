<center>
	<div class="container" style="margin-top: 20px;">
		<div class="card col-6 text-left">
			<div class="card-header">
				<h5 class="card-title">Nuevo</h5>
			</div>
			<form method="post" action="<?=base_url(); ?>Canciones/actionEditarCancion">
				<div class="card-body">
					<div class="form-row">
						<div class="col-12">
							<input type="hidden" name="id_cancion" value="<?=$cancion->id_cancion;?>">
							<label for="titulo">Titulo cación</label>
							<input type="text" name="titulo" class="form-control" value="<?=$cancion->titulo ?>">
						</div>
						<div class="col-12">
							<label>Album del artista</label>
							<select name="id_album" class="form-control">
								<option disabled selected>Select Album</option>
								<?php foreach ($albumnes as $A): ?>
									<option value="<?=$A->id_album;?>" <?=$A->id_album == $cancion->id_album ? 'selected': '' ?>><?=$A->titulo_album." ".$A->seudo ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-12">
							<label>Fecha de lanzamiento</label>
							<input type="date" name="lanzamiento" class="form-control" value="<?=$cancion->lanzamiento ?>">
						</div>
						<div class="col-12">
							<label>Duración</label>
							<input type="time" name="duracion" class="form-control" value="<?=$cancion->duracion ?>">
						</div>
						<div class="col-12">
							<label>Genero</label>
							<select name="id_genero" class="form-control">
								<option disabled selected>Select genero</option>
								<?php foreach ($generos as $G): ?>
									<option value="<?=$G->id_genero;?>"<?=$G->id_genero == $cancion->id_genero ? 'selected': '' ?>><?=$G->genero;?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-12">
							<label>Old file</label>
							<audio class="form-control" src="<?=base_url(); ?>audio/<?=$cancion->file; ?>" preload="none" controls></audio>
						</div>
						<div class="col-12">
							<label>New Archivo</label>
							<input type="file" name="file" class="form-control" >
						</div>
					</div>
				</div>
				<div class="card-footer">
					<input type="submit" value="Guardar" class="btn btn-primary">
					<a href="<?=base_url(); ?>Canciones/" class="btn btn-secondary">Volver</a>
				</div>
			</form>
		</div>
	</div>
</center>