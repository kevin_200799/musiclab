<style type="text/css">
	img{
		width: 175px;
		height: 180px;
		margin: 10px;
		padding: 10px;
	}
	#paragraf{
		
		width: 700px;
		margin: 10px;
		padding: 10px;
	}
</style>
<center>
	<div class="container" style="margin-top: 30px;">
		
		<div class="form-row">
			<?php foreach ($cover as $C): ?>
				<div class="card col-4">
					<div class="card-header">
						<h5 class="card-title"><?=$C->titulo_album ;?></h5>
					</div>
					<div class="card-body">
						<img src="<?=base_url(); ?>img/cover/<?=$C->portada?>">
						<p>
							<b>Fecha de lanzamiento:<?=$C->f_lanzamiento ;?></b>
						</p>
					</div>
					<div class="card-footer">
						<a href="<?=base_url();?>Canciones/CancionesByArtist/<?=$C->id_artista;?>" class="btn btn-success">Ver canciones</a>
						<a href="<?=base_url(); ?>Albumnes_Controller/getAlbum/<?=$C->id_album?>" class="btn btn-primary">Editar información</a>
						<a href="" class="btn btn-danger">Eliminar</a>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</center>