<!DOCTYPE html>
<html>
<head>
	<title>Bienvenido / Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>font/css/all.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
</head>
<body style="background-image: url('<?php echo base_url(); ?>assets/bg2.png');background-size: cover;">
	<center>
		<form method="post" action="<?=base_url();?>Login/Logear">
			<!-- <input type="hidden" name="KeyTokenIDFacker" value="<?=md5("powered by MusiClab.com PA Sv 2019")?>"> -->
			<div class="container mt-5">
				<div class="card col-md-5">
					<div class="card-header">
						<img class="img-card" style="width: 200px;height: 200px;" src="<?=base_url();?>img/SystemRecovery/musiclabV0.001Unpublished.png">
						<h3 class="card-title">Bienvenido a MusiClab</h3>
					</div>
					<div class="card-body text-left">
						<div class="form-row">
							<h4 class="card-subtitle text-center">Inicia Sesión</h4>
						</div>

						<div class="form-row">
							<div class="col-md-12">
								<label for="usuario">Usuario</label>
							</div>
							<div class="col-md-12">
								<input id="usuario" type="text" name="usuario" class="form-control">
							</div>
							<div class="col-md-12">
								<label for="password">Contraseña</label>
							</div>
							<div class="col-md-12">
								<div class="input-group mb-2">
									<div class="input-group-prepend">
										<div class="input-group-text">
											<button id="show_password" class="btn btn-sm btn-primary right" type="button" onclick="mostrarPassword()">
												<i id="icon" class="fas fa-eye-slash"></i>
											</button>
										</div>
									</div>
									<input type="password" id="password" name="contrasena" class="form-control">
								</div>
							</div>
						</div>

						<span class="text-danger text-center">
							<?=$this->session->flashdata('msg'); ?>
						</span>

					</div>
					<div class="card-footer">
						<button type="submit" class="btn btn-block btn-primary">/* ENTRAR */</button>
					</div>
				</div>
			</div>
		</form>
	</center>
</body>
<script type="text/javascript">
	function mostrarPassword(){
		var cambio = document.getElementById("password");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('#icon').removeClass("fas fa-eye-slash").addClass("fas fa-eye");
		}else{
			cambio.type = "password";
			$('#icon').removeClass("fas fa-eye").addClass("fas fa-eye-slash");
		}
	}
</script>
</html>