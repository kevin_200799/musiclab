<?php 
class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("LoginModel");
	}

	public function index()
	{
		$this->load->view('Login');
	}

	public function Logear()
	{
		$usuario = $this->input->post('usuario',true);
		$contrasena = base64_encode($this->input->post('contrasena',true));
		$validate = $this->LoginModel->Exist_User($usuario,$contrasena);

		if ($validate->num_rows() > 0) {
			$data = $validate->row_array();
			$idusuario = $data['id_user'];
			$usuario = $data['user'];
			$contrasena = $data['password'];
			$fecha_nacimiento = $data['fecha_nacimiento'];
			$nombres = $data['nombres'];
			$apellidos = $data['apellidos'];
			$rol = $data['rol'];

			$datos = array(
				'id_user' => $idusuario,
				'user' => $usuario,
				'password' => $contrasena,
				'fecha_nacimiento' => $fecha_nacimiento,
				'nombres' => $nombres,
				'apellidos' => $apellidos,
				'rol' => $rol,
				'is_logued_in' => TRUE
			);
			$this->session->set_userdata($datos);
			if ($this->session->userdata('rol')=='Administrador') {
				redirect(base_url().'Inicio_Ctrl/index');
			}elseif ($this->session->userdata('rol')=='Usuarios') {
				redirect('Inicio_Ctrl/index');
			}
		}else{
			echo $this->session->set_flashdata('msg','Usuario o contraseña Incorrectos!! :(');
			redirect(base_url().'Login');
		}
	}

	public function Logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'Login');
	}

}
?>