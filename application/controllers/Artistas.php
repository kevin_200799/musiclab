<?php
class Artistas extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Artistas_Modelo');
	}

	public function Index()
	{
		$data = array(
			'title' => 'Artistas', 
			'view' => 'Artistas/Artistas', 
			'data_view' => array());
		$artistas= $this->Artistas_Modelo->Artistas();
		$data['artistas'] =$artistas;
		$this->load->view('Markup/Main_View',$data);
	}
	public function Nuevo()
	{
		$data = array(
			'title' => 'Artistas', 
			'view' => 'Artistas/Nuevo', 
			'data_view' => array());
		$disquera= $this->Artistas_Modelo->Disquera();
		$data['disquera'] =$disquera;
		$this->load->view('Markup/Main_View',$data);
	}
	private function savefile()
	{
		# code...
	}

	public function InsertNew()
	{
		if ($this->input->is_ajax_request()) {
			$data = array(
				'foto' => $this->input->Post('foto'), 
				'nombres' => $this->input->Post('nombres'), 
				'apellidos' => $this->input->Post('apellidos'), 
				'seudo' => $this->input->Post('seudo'), 
				'nacimiento' => $this->input->Post('nacimiento'), 
				'biografia' => $this->input->Post('biografia'), 
				'id_disquera' => $this->input->Post('id_disquera'));
			if ($this->Artistas_Modelo->InsertNew($data)) {
				echo json_encode(array('success'=>1));
			}else{
				echo json_encode(array('success'=>0));
			}
		}else{
			echo "No se puede acceder";
		}
	}

}
?>