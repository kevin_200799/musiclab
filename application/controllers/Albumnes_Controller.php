<?php 
class Albumnes_Controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Albumnes_Model');
	}

	public function GetAllCoverArtist($id_artista)
	{
		$data = array(
			'title' => 'Musica',
			'view' => 'Canciones/SelectCanciones.php',
			'data_view' => array());
		$cover= $this->Albumnes_Model->GetAllCoverArtist($id_artista);
		$data['cover'] =$cover;
		$this->load->view('Markup/Main_View',$data);
	}
}
?>