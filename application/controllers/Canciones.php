<?php 
class Canciones extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Canciones_Model');
	}

	public function Index()
	{
		$data = array(
			'title' => 'Musica',
			'view' => 'Canciones/Canciones',
			'data_view' => array());
		$canciones= $this->Canciones_Model->Canciones();
		$data['canciones'] =$canciones;
		$this->load->view('Markup/Main_View',$data);
	}

	public function SelectAllArtist($id)
	{
		$data = array(
			'title' => 'Musica',
			'view' => 'Canciones/SelectCanciones.php',
			'data_view' => array());
		$discografia= $this->Canciones_Model->SelectAllArtist($id);
		$data['discografia'] =$discografia;
		$this->load->view('Markup/Main_View',$data);
	}

	public function nuevaCancion()
	{
		$data = array(
			'title' => 'Nueva Canción',
			'view' => 'Canciones/Nuevo',
			'data_view' => array()
		);	
		$data['albumnes'] = $this->Albumnes_Model->getAll();
		$data['generos'] = $this->GeneroModel->getAll();
		$this->load->view('Markup/Main_View',$data);
	}

	public function agregarCancion()
	{
		$data = array(
			'titulo' => $_POST['titulo'], 
			'id_album' => $_POST['id_album'], 
			'lanzamiento' => $_POST['lanzamiento'], 
			'duracion' => $_POST['duracion'], 
			'id_genero' => $_POST['id_genero'], 
			'file' => $_POST['file']
		);
		$this->Canciones_Model->agregarCancion($data);
		return redirect(base_url()."Canciones/Index");
	}

	public function editarCancion($id)
	{
		$data = array(
			'title' => 'Nueva Canción',
			'view' => 'Canciones/Editar',
			'data_view' => array()
		);	
		$data['cancion'] = $this->Canciones_Model->getCancion($id);
		$data['albumnes'] = $this->Albumnes_Model->getAll();
		$data['generos'] = $this->GeneroModel->getAll();
		$this->load->view('Markup/Main_View',$data);
	}

	public function actionEditarCancion()
	{
		$data = array(
			'id_cancion' => $_POST['id_cancion'],
			'titulo' => $_POST['titulo'], 
			'id_album' => $_POST['id_album'], 
			'lanzamiento' => $_POST['lanzamiento'], 
			'duracion' => $_POST['duracion'], 
			'id_genero' => $_POST['id_genero'], 
			'file' => $_POST['file']
		);
		$this->Canciones_Model->editarCancion($data);
		return redirect(base_url()."Canciones/Index");
	}

	public function Reporte()
	{
		$data['canciones'] = $this->Canciones_Model->Canciones();
		$this->load->view('Canciones/Reporte',$data);

		$html = $this->output->get_output();
		$this->dompdf->loadHtml($html);
		// $this->dompdf->setPaper('100','portrait');
		$this->dompdf->render();
		$this->dompdf->stream('Canciones.pdf',array('Attachment=>0'));
	}

	public function eliminarCancion($id)
	{
		$this->Canciones_Model->eliminarCancion($id);
		return redirect(base_url()."Canciones/Index");
	}

	public function CancionesByArtist($id)
	{
		$data = array(
			'title' => 'Canciones',
			'view' => 'Canciones/CancionesByArtist',
			'data_view' => array() 
		);
		$data['lista']=$this->Canciones_Model->CancionesByArtist($id);
		$this->load->view('Markup/Main_View',$data);
	}
}
?>